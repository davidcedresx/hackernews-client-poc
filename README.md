# Hacker News Client

## How to run

    composer up

Then visit `localhost:3000`

## How to force article fetch

I failed to implement it, right now the best option is just configuring the worker with `0 * * * * *` to make it run every minute
